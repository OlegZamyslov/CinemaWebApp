<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Стартовая</title>
    <link rel="stylesheet" type="text/css" href="/pages/menuStyle.css"/>
    <link rel="stylesheet" type="text/css" href="/pages/adminTable.css"/>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

</head>
<body>
<c:import url="/pages/common/head.jsp"/>

<c:if test="${sessionScope.currentsessions !=null}">
    <table border="2">
        <tr>
            <th>№</th>
            <th>Название</th>
            <th>Зал</th>
            <th>Дата</th>
            <th>Время</th>
            <th>Цена, грн</th>
            <th></th>
        </tr>

        <c:forEach items="${currentsessions}" var="session" varStatus="counter">
            <tr>
                <td> ${counter.count}</td>
                <td> ${session.movie.title}</td>
                <td> ${session.hall.name}</td>
                <fmt:parseDate value="${session.sessionDate}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
                <td><fmt:formatDate type="date" pattern="dd/MM" value="${parsedDateTime}" /></td>
                <fmt:parseDate value="${session.sessionDate}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
                <td><fmt:formatDate type="time" pattern="HH:mm" value="${parsedDateTime}" /></td>
                <td> ${session.price}</td>
                <td><a href="/buyTicket?id=${session.id}">Купить билеты</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>


