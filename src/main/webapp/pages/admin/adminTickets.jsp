<%--
  Created by IntelliJ IDEA.
  User: zamyslov
  Date: 13.06.2017
  Time: 9:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/pages/adminTable.css" media="screen" type="text/css"/>
</head>
<body>
<c:import url="adminHead.jsp"/><br/>
<form name="tickets" method="post" action="${pageContext.servletContext.contextPath}/tickets">
    <l></l><input type="submit" name="delete" value="Удалить">
    <c:if test="${sessionScope.tickets !=null}">
        <table border="2">
            <tr>
                <th>№</th>
                <th></th>
                <th>Пользователь</th>
                <th>Ряд</th>
                <th>Место</th>
                <th>Сеанс</th>
            </tr>

            <c:forEach items="${tickets}" var="ticket" varStatus="counter">
                <tr>
                    <td> ${counter.count}</td>
                    <td><input type="checkbox" name="ticketId" value="${ticket.id}"></td>
                    <td> ${ticket.user.login}</td>
                    <td> ${ticket.row}</td>
                    <td> ${ticket.seat}</td>
                    <td> ${ticket.session.sessionDate}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

</form>

</body>
</html>
