<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 02.07.2017
  Time: 15:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/pages/adminTable.css" type="text/css"/>
</head>
<body>
<c:import url="adminHead.jsp"/><br/>
<form name="addhall" method="post" action="${pageContext.servletContext.contextPath}/addhall">

    <c:if test="${sessionScope.hallstructure !=null}"><br>
        <l></l>
        <table border="2">
            <c:forEach items="${sessionScope.hallstructure}" var="raw" varStatus="counter">
                <tr>
                    <td> ${counter.count}</td>
                    <td><input type="checkbox" name="rawId" value="${counter.count}"></td>
                    <td>
                        <c:forEach var="seat" begin="1" end="${raw}">
                            <seat>${seat}</seat>
                        </c:forEach><br>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <output>Мест в ряду</output>
    <input type="text" name="Мест в ряду">
    <input type="submit" name="addrow" value="Добавить ряд">
    <input type="submit" name="deleterow" value="Удалить ряд">
    <input type="submit" name="savehall" value="Сохранить зал">
</form>
</body>
</html>
