<%--
  Created by IntelliJ IDEA.
  User: zamyslov
  Date: 13.06.2017
  Time: 9:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Админка</title>
    <link rel="stylesheet" type="text/css" href="/pages/menuStyle.css" />
</head>
<body>


<div id="ii">
    <ul id="menu">
        <li><a href="/index.jsp">Главная</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/selecthalls">Залы</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/selectmovies">Фильмы </a>
        <li><a href="${pageContext.servletContext.contextPath}/selectsessions">Сеансы</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/selectusers">Пользователи</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/selecttickets">Билеты</a></li>
    </ul>
</div>
</body>
</html>
