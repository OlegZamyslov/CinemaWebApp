<%--
  Created by IntelliJ IDEA.
  User: zamyslov
  Date: 13.06.2017
  Time: 9:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/pages/adminTable.css" media="screen" type="text/css"/>
</head>
<body>
<c:import url="adminHead.jsp"/><br/>
<form name="movies" method="post" action="${pageContext.servletContext.contextPath}/moviesCRUD">
    <l></l><input type="submit" name="add" value="Добавить">
    <input type="submit" name="update" value="Редактировать">
    <input type="submit" name="delete" value="Удалить">

    <c:if test="${sessionScope.movies !=null}">
        <table border="2">
            <tr>
                <th>№</th>
                <th></th>
                <th>Название</th>
                <th>Описание</th>
                <th>Длительность</th>
            </tr>

            <c:forEach items="${movies}" var="movie" varStatus="counter">
                <tr>
                    <td> ${counter.count}</td>
                    <td><input type="checkbox" name="movieId" value="${movie.id}"></td>
                    <td> ${movie.title}</td>
                    <td> ${movie.description}</td>
                    <td> ${movie.duration}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

</form>
</body>
</html>
