<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 16.06.2017
  Time: 19:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/pages/login.css" type="text/css"/>
</head>
<body>
<c:import url="adminHead.jsp"/><br/>
<div id="inputform">
    <c:choose>
        <c:when test="${sessionScope.hall!=null}">
            <h1>Редактировать зал</h1>
        </c:when>
        <c:otherwise>
            <h1>Новый зал</h1>
        </c:otherwise>
    </c:choose>
    <fieldset>
        <form name="addhall" method="post" action="${pageContext.servletContext.contextPath}/addhall">
            <output>Название зала</output>
            <c:choose>
                <c:when test="${sessionScope.hall!=null}">
                    <input type="text" name="Имя зала" value="${hall.name}">
                    <input type="submit" name="addstructure" value="Редактировать структуру">
                </c:when>
                <c:otherwise>
                    <input type="text" name="Имя зала">
                    <input type="submit" name="addhall" value="Добавить зал"><br>
                </c:otherwise>
            </c:choose>
        </form>
    </fieldset>
</div>

</body>
</html>
