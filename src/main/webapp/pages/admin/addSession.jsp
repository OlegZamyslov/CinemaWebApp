<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 16.06.2017
  Time: 19:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/pages/login.css" media="screen" type="text/css"/>
</head>
<body>
<%--<c:import url="adminHead.jsp"/><br/>--%>
<div id="inputform">
    <h1>Новый сеанс</h1>
    <fieldset>

        <form name="addsession" method="post" action="${pageContext.servletContext.contextPath}/addsession">
            <output>Выберите зал</output>
            </br>
            <select name="halls">
                <c:forEach items="${sessionScope.halls}" var="hall">
                    <option value="${hall.id}">${hall.name}</option>
                </c:forEach><br>
            </select>
            <br>
            <output>Выберите фильм</output>
            </br>
            <select name="movies">
                <c:forEach items="${sessionScope.movies}" var="movie">
                    <option value="${movie.id}">${movie.title}</option>
                </c:forEach><br>
            </select>
            <br>
            <output>Выберите дату сеанса</output>
            </br>
            <input type="date" name="sessionDate">
            <br>
            <output>Выберите время сеанса</output>
            </br>
            <input type="time" name="sessionTime">
            <br>
            <output>Выберите цену билета</output>
            </br>
            <input type="text" name="price">
            <input type="submit" name="savesession" value="Сохранить сеанс">
        </form>
    </fieldset>
</div>
</body>
</html>
