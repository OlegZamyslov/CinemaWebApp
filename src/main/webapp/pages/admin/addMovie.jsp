<%--
  Created by IntelliJ IDEA.
  User: zamyslov
  Date: 12.06.2017
  Time: 12:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%request.setCharacterEncoding("UTF-8");%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" href="/pages/login.css" media="screen" type="text/css"/>
    <title>Registration</title>
</head>
<body>

<div id="inputform">
    <h1>Новый фильм</h1>
    <fieldset>
        <form name="newMovieForm" enctype="multipart/form-data" method="post"
              action="${pageContext.servletContext.contextPath}/addmovie">
            <c:choose>
                <c:when test="${sessionScope.movie !=null}">
                    <output>Название фильма</output></br>
                    <input type="text" name="name" value="${movie.title}">
                    <br><output>Описание фильма</output></br>
                    <textarea name="descr">${movie.description}</textarea><br>
                    <output>Длительность фильма, мин</output></br>
                    <input type="text" name="duration" value="${movie.duration}"><br>
                    <output>Постер фильма</output></br>
                    <input type="file" name="poster" accept="image/*" value="${sessionScope.path}">
                    <input type="submit" name="update" value="Изменить">
                </c:when>
                <c:otherwise>
                    <output>Название фильма</output></br>
                    <input type="text" name="name">
                    <br><output>Описание фильма</output></br>
                    <textarea name="descr"></textarea><br>
                    <output>Длительность фильма, мин</output></br>
                    <input type="text" name="duration"><br>
                    <output>Постер фильма</output></br>
                    <input type="file" name="poster" accept="image/*">
                    <input type="submit" name="save" value="Сохранить">
                </c:otherwise>
            </c:choose>
        </form>
    </fieldset>
</div>


</body>
</html>
