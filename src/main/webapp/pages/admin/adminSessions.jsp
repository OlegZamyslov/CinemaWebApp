<%--
  Created by IntelliJ IDEA.
  User: zamyslov
  Date: 13.06.2017
  Time: 9:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/pages/adminTable.css" media="screen" type="text/css"/>
</head>
<body>
<c:import url="adminHead.jsp"/><br/>
<form name="sessions" method="post" action="${pageContext.servletContext.contextPath}/sessions">
    <l></l><input type="submit" name="add" value="Добавить">
    <input type="submit" name="delete" value="Удалить">

    <c:if test="${sessionScope.sessions !=null}">
        <table border="2">
            <tr>
                <th>№</th>
                <th></th>
                <th>Фильм</th>
                <th>Зал</th>
                <th>Дата</th>
                <th>Цена</th>
            </tr>

            <c:forEach items="${sessionScope.sessions}" var="session" varStatus="counter">
                <tr>
                    <td> ${counter.count}</td>
                    <td><input type="checkbox" name="sessionId" value="${session.id}"></td>
                    <td> ${session.movie.title}</td>
                    <td> ${session.hall.name}</td>
                    <td> ${session.sessionDate}</td>
                    <td> ${session.price}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

</form>
</body>
</html>
