<%--
  Created by IntelliJ IDEA.
  User: zamyslov
  Date: 13.06.2017
  Time: 9:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/pages/adminTable.css" media="screen" type="text/css"/>
</head>
<body>
<c:import url="adminHead.jsp"/><br/>
<form name="users" method="post" action="${pageContext.servletContext.contextPath}/users">
    <l></l><input type="submit" name="add" value="Добавить">
    <input type="submit" name="delete" value="Удалить">
    <c:if test="${sessionScope.users !=null}">
        <table border="2">
            <tr>
                <th>№</th>
                <th></th>
                <th>Логин</th>
                <th>Пароль</th>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Дата рождения</th>
                <th>Роль</th>
            </tr>

            <c:forEach items="${users}" var="user" varStatus="counter">
                <tr>
                    <td> ${counter.count}</td>
                    <td><input type="checkbox" name="userId" value="${user.id}"></td>
                    <td> ${user.login}</td>
                    <td> ${user.pass}</td>
                    <td> ${user.lastName}</td>
                    <td> ${user.firstName}</td>
                    <td> ${user.BDay}</td>
                    <td> ${user.role.name}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

</form>

</body>
</html>
