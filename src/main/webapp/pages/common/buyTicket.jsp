<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 28.06.2017
  Time: 12:53
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/pages/menuStyle.css"/>
    <link rel="stylesheet" type="text/css" href="/pages/adminTable.css"/>
    <title>Фильмы</title>
</head>
<body>
<c:import url="head.jsp"/>

<form name="tickets" method="post" action="${pageContext.servletContext.contextPath}/buyTicket">
    <c:if test="${sessionScope.structure !=null}">
        <l></l><seatdemo>Доступные</seatdemo>
        <seat2demo>Купленные</seat2demo>
        </br></br></br>
        <table border="2">
            <c:forEach items="${sessionScope.structure}" var="raw" varStatus="counterrow">
                <%--<seat3>Ряд #${counterrow.count}</seat3>--%>
                <tr>
                    <td><seat3>Ряд#${counterrow.count}</seat3></td>
                    <td><c:forEach items="${raw}" var="seat" varStatus="counterseat">
                        <c:if test="${seat ==0}">
                            <seat><input type="checkbox" name="seatId"
                                         value="${counterrow.count}/${counterseat.count}"></br>
                                    ${counterseat.count}</seat>
                        </c:if>
                        <c:if test="${seat !=0}">
                            <seat2><input type="checkbox" name="seatId"
                                          value="${counterrow.count}/${counterseat.count}" disabled></br>
                                    ${counterseat.count}</seat2>
                        </c:if>

                    </c:forEach><br></td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <l></l><input type="submit" name="savetickets" value="Купить билеты">

</form>

</body>
</html>
