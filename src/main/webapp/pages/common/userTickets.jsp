<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 11.06.2017
  Time: 17:41
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/pages/menuStyle.css"/>
    <link rel="stylesheet" type="text/css" href="/pages/adminTable.css"/>
    <title>Фильмы</title>
</head>
<body>
<c:import url="head.jsp"/>
<form name="tickets" method="post" action="${pageContext.servletContext.contextPath}/buyTicket">
    <l></l>
    <input type="submit" name="printtickets" value="Распечатать">

    <c:if test="${sessionScope.tickets !=null}">
        <table border="2">
            <tr>
                <th>№</th>
                <th></th>
                <th>Фильм</th>
                <th>Зал</th>
                <th>Дата</th>
                <th>Время</th>
                <th>Цена, грн</th>
            </tr>

            <c:forEach items="${tickets}" var="tickets" varStatus="counter">
                <tr>
                    <td> ${counter.count}</td>
                    <td><input type="checkbox" name="ticketId" value="${tickets.id}"></td>
                    <td> ${tickets.session.movie.title}</td>
                    <td> ${tickets.session.hall.name}</td>
                    <fmt:parseDate value="${tickets.session.sessionDate}" pattern="yyyy-MM-dd'T'HH:mm"
                                   var="parsedDateTime" type="both"/>
                    <td><fmt:formatDate type="date" pattern="dd/MM" value="${parsedDateTime}"/></td>
                    <fmt:parseDate value="${tickets.session.sessionDate}" pattern="yyyy-MM-dd'T'HH:mm"
                                   var="parsedDateTime" type="both"/>
                    <td><fmt:formatDate type="time" pattern="HH:mm" value="${parsedDateTime}"/></td>
                    <td> ${tickets.session.price}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</form>
</body>
</html>
