<%--
  Created by IntelliJ IDEA.
  User: Alexandr
  Date: 05.10.2016
  Time: 8:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Head</title>
    <link rel="stylesheet" type="text/css" href="/pages/menuStyle.css"/>
</head>
<body>

<ul id="menu">

    <li><a href="/index.jsp">На главную</a></li>
    <li>
        <a href="">Расписание</a>
        <ul>
            <li><a href="/schedule?term=today">На сегодня</a></li>
            <li><a href="/schedule?term=week">На неделю</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/schedule">Фильмы</a></li>
    <c:choose>
        <c:when test="${sessionScope.user !=null}">
            <c:if test="${sessionScope.user.role.name eq 'User'}">
            <li>
                <a href=""><c:out value="${user.login}"/></a>
                <ul>
                    <li><a href="${pageContext.servletContext.contextPath}/adduser?curruserid=${sessionScope.user.id}">Данные</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/usertickets?id=${sessionScope.user.id}">Билеты</a></li>
                </ul>
            </li>
            </c:if>
            <li><a href="${pageContext.servletContext.contextPath}/exit">Выход</a></li>
            <c:if test="${sessionScope.user.role.name eq 'Admin'}">
                <li><a href="${pageContext.servletContext.contextPath}/pages/admin/adminArea.jsp">Админка</a></li>
            </c:if>
        </c:when>
        <c:otherwise>
            <li><b></b></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/common/login.jsp">Вход</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/common/addUser.jsp">Регистрация</a></li>
        </c:otherwise>
    </c:choose>
</ul>

</body>
</html>
