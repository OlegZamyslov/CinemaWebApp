<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 11.06.2017
  Time: 17:37
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%request.setCharacterEncoding("UTF-8");%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" href="/pages/login.css" media="screen" type="text/css"/>
    <title>Login</title>
</head>
<body>
<div id="inputform">
    <h1>Авторизация на сайте</h1>
    <fieldset>
        <form name="login" method="post" action="${pageContext.servletContext.contextPath}/login">
            <output>Логин</output>
            </br>
            <input type="text" name="Логин">
            <br>
            <output>Пароль</output>
            </br>
            <input type="password" name="Пароль">
            <input type="submit" value="Войти">
        </form>
    </fieldset>
</div>
</body>
</html>
