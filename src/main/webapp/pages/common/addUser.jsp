<%--
  Created by IntelliJ IDEA.
  User: zamyslov
  Date: 12.06.2017
  Time: 12:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" href="/pages/login.css" media="screen" type="text/css"/>
    <title>Registration</title>
</head>
<body>

<div id="inputform">
    <h1>Регистрация на сайте</h1>
    <fieldset>
        <form name="newUserForm" method="post" action="${pageContext.servletContext.contextPath}/adduser">
            <c:if test="${sessionScope.currusers ==null}">
                <output>Логин</output>
                <br><input type="text" name="Логин"></br>
                <output>Пароль</output>
                <br>
                <input type="password" name="Пароль">
                </br>
                <output>Имя</output>
                <br>
                <input type="text" name="Имя">
                </br>
                <output>Фамилия</output>
                <input type="text" name="Фамилия">
                <output>Электронная почта</output>
                <br>
                <input type="text" name="Email">
                </br>
                <output>Дата рождения</output>
                <input type="date" name="bdate">
                <c:if test="${sessionScope.user != null}">
                    <c:if test="${sessionScope.user.role.name eq 'Admin'}">
                        <select name="role">
                            <option value="Admin">Admin</option>
                            <option value="User">User</option>
                        </select>
                    </c:if>
                </c:if>
                <input type="submit" name="register" value="Регистрация">
            </c:if>
            <c:if test="${sessionScope.currusers !=null}">
                <output>Логин</output>
                <input type="text" name="Логин" value="${sessionScope.currusers.login}"></br>
                <output>Пароль</output>
                <input type="password" name="Пароль" value="${sessionScope.currusers.pass}">
                </br>
                <output>Имя</output>
                <input type="text" name="Имя" value="${sessionScope.currusers.firstName}"}>
                <output>Фамилия</output>
                <input type="text" name="Фамилия" value="${sessionScope.currusers.lastName}">
                <output>Электронная почта</output>
                <input type="text" name="Email" value="${sessionScope.currusers.email}">
                <output>Дата рождения</output>
                <input type="date" name="bdate" value="${sessionScope.currusers.BDay}">
                <input type="submit" name="change" value="Изменить">
            </c:if>
        </form>
    </fieldset>
</div>
</body>
</html>
