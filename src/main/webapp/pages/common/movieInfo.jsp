<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 11.06.2017
  Time: 17:41
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/pages/adminTable.css"/>
    <link rel="stylesheet" type="text/css" href="/pages/menuStyle.css"/>
    <title>Фильмы</title>
</head>
<body>
<c:import url="head.jsp"/>
<table border="2">
    <tr>
        <th>Название</th>
        <th></th>
        <th>Описание</th>
        <th>Время</th>
    </tr>

    <tr>
        <td>${currmovie.title}</td>
        <td><img src="/image?file=${currmovie.id}"/></td>
        <td>${currmovie.description}</td>
        <td>Длительность:${currmovie.duration}</td>
    </tr>
</table>


</body>
</html>
