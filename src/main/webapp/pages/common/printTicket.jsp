<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 28.06.2017
  Time: 12:53
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/pages/adminTable.css"/>
    <title>Купленные билеты</title>
</head>
<body>
<c:import url="head.jsp"/>
<l></l>
Ваши билеты: </br>
<c:if test="${sessionScope.myTickets !=null}">
    <table border="2">
        <c:forEach items="${sessionScope.myTickets}" var="ticket">
            <tr>
                <td> Билет №${ticket.session.id}/${ticket.id}</td>
                <td> ${ticket.session.movie.title}</td>
                <td><img src="/image?file=${ticket.session.movie.id}" width="60" height="100"/></td>
                <td> ${ticket.session.hall.name}</td>
                <fmt:parseDate value="${ticket.session.sessionDate}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime"
                               type="both"/>
                <td><fmt:formatDate type="date" pattern="dd/MM" value="${parsedDateTime}"/></td>
                <fmt:parseDate value="${ticket.session.sessionDate}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime"
                               type="both"/>
                <td><fmt:formatDate type="time" pattern="HH:mm" value="${parsedDateTime}"/></td>
                <td> ${ticket.session.price}</td>
            </tr>
        </c:forEach>
    </table>
</c:if>

</body>
</html>
