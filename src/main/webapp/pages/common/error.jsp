<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 02.07.2017
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/pages/menuStyle.css"/>
    <link rel="stylesheet" type="text/css" href="/pages/error.css"/>
    <title>Ошибка</title>
</head>
<body>
<c:import url="head.jsp"/>
<div id="inputform">
    <h1>Ошибка</h1>
    <fieldset>
        <if test="${sessionScope.errormessage ==null}">
            <output>${sessionScope.errormessage}</output>
        </if>
    </fieldset>
</div>

</body>
</html>
