package model;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class User extends Entity<Integer>{
    String login;
    String pass;
    String firstName;
    String lastName;
    Role role;
    String email;
    LocalDate BDay;
}
