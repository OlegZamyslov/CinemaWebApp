package model;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Ticket extends Entity<Integer>{
    User user;
    int row;
    int seat;
    Session session;

}
