package model;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Hall extends Entity<Integer>{
    String name;
    int [] seats;
}
