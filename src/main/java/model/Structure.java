package model;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Structure extends Entity<Integer>{
    int Hall_id;
    int [] seats;
}
