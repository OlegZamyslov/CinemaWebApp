package model;

import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Session extends Entity<Integer>{
    Movie movie;
    Hall hall;
    LocalDateTime sessionDate;
    int price;
}
