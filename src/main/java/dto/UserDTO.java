package dto;

import lombok.*;
import model.Entity;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserDTO extends Entity<Integer> {
    String login;
    String pass;
    String firstName;
    String lastName;
    RoleDTO role;
    String email;
    LocalDate BDay;
}
