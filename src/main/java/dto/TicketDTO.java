package dto;

import lombok.*;
import model.Entity;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class TicketDTO extends Entity<Integer> {
    UserDTO user;
    int row;
    int seat;
    SessionDTO session;

}
