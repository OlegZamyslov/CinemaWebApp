package dto;

import lombok.*;
import model.Entity;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class HallDTO extends Entity<Integer> {
    String name;
    int [] seats;
}
