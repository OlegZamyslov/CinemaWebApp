package dto;

import lombok.*;
import model.Entity;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StructureDTO extends Entity<Integer> {
    int hall_id;
    int [] seats;
}
