package dto;

import lombok.*;
import model.Entity;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class RoleDTO extends Entity<Integer> {
    String name;

}
