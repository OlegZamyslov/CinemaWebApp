package dto;

import lombok.*;
import model.Entity;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class SessionDTO extends Entity<Integer> {
    MovieDTO movie;
    HallDTO hall;
    LocalDateTime sessionDate;
    int price;
}
