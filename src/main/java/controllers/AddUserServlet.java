package controllers;

import dto.UserDTO;
import service.impl.RoleServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


@WebServlet(name = "AddUserServlet", urlPatterns = "/adduser")
public class AddUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("curruserid");
        if (id != null) {
            UserDTO user = UserServiceImpl.getInstance().getById(Integer.valueOf(id));
            request.getSession().setAttribute("currusers", user);
            response.sendRedirect(request.getContextPath() + "/pages/common/addUser.jsp");
        } else {
            String login = request.getParameter("Логин");
            String pass = request.getParameter("Пароль");
            String lastName = request.getParameter("Имя");
            String firstName = request.getParameter("Фамилия");
            String email = request.getParameter("Email");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String date = request.getParameter("bdate");
            LocalDate bDate;
            if (date.equals("")) {
                bDate = LocalDate.now();
            } else {
                bDate = LocalDate.parse(date, formatter);
            }
            if (request.getParameter("change") != null) {
                UserDTO currentUser = (UserDTO) request.getSession().getAttribute("user");
                UserDTO newUser = new UserDTO(login, pass, firstName, lastName, RoleServiceImpl.getInstance().getByName("User"), email, bDate);
                newUser.setId(currentUser.getId());
                UserServiceImpl.getInstance().update(newUser);
                response.sendRedirect(request.getContextPath() + "/index.jsp");
            } else {
                UserDTO user = UserServiceImpl.getInstance().getByLogin(login);
                if (user != null) {
                    request.getSession().setAttribute("errormessage", "Этот логин уже зарегистрирован");
                    response.sendRedirect(request.getContextPath() + "/pages/common/error.jsp");
                } else {
                    UserDTO currentUser = (UserDTO) request.getSession().getAttribute("user");
                    if (currentUser != null && currentUser.getRole().getName().equals("Admin")) {
                        String role = request.getParameter("role");
                        UserDTO newUser = new UserDTO(login, pass, firstName, lastName, RoleServiceImpl.getInstance().getByName(role), email, bDate);
                        UserServiceImpl.getInstance().save(newUser);
                        List<UserDTO> users = UserServiceImpl.getInstance().getAll();
                        request.getSession().setAttribute("users", users);
                        response.sendRedirect(request.getContextPath() + "/pages/admin/adminUsers.jsp");
                    } else {
                        UserDTO newUser = new UserDTO(login, pass, firstName, lastName, RoleServiceImpl.getInstance().getByName("User"), email, bDate);
                        UserServiceImpl.getInstance().save(newUser);
                        request.getSession().setAttribute("user", newUser);
                        response.sendRedirect(request.getContextPath() + "/pages/common/login.jsp");
                    }
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
