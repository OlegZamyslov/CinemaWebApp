package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import dto.UserDTO;
import model.Session;
import service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "AddSessionServlet", urlPatterns = "/addsession")
public class AddSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int hallID = Integer.valueOf(request.getParameter("halls"));
        HallDTO hallDTO = HallServiceImpl.getInstance().getById(hallID);
        int movieID = Integer.valueOf(request.getParameter("movies"));
        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(movieID);
        String sessionDate = request.getParameter("sessionDate");
        String sessionTime = request.getParameter("sessionTime");
        int price = Integer.valueOf(request.getParameter("price"));
        try {
            DateTimeFormatter formatterTime = DateTimeFormatter.ISO_LOCAL_TIME;
            LocalTime time = LocalTime.parse(sessionTime, formatterTime);
            DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date = LocalDate.parse(sessionDate, formatterDate);
            LocalDateTime dateTime = date.atTime(time);
            SessionDTO sessionDTO = new SessionDTO(movieDTO,hallDTO,dateTime,price);
            SessionServiceImpl.getInstance().save(sessionDTO);
            List<SessionDTO> sessions = SessionServiceImpl.getInstance().getAll();
            request.getSession().setAttribute("sessions", sessions);
            response.sendRedirect(request.getContextPath() + "/pages/admin/adminSessions.jsp");
        }catch (Exception e) {
            request.getSession().setAttribute("errormessage", "Введите правильные даты и время");
            response.sendRedirect(request.getContextPath() + "/pages/common/error.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
