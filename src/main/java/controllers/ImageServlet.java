package controllers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


@WebServlet(name = "ImageServlet", urlPatterns = "/image")
public class ImageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = "c:/image/" + request.getParameter("file") + ".jpg";
        if ((new File(path)).exists()) {
            try (InputStream is = new FileInputStream(path);
                 BufferedInputStream in = new BufferedInputStream(is)) {
                int ch;
                while ((ch = in.read()) != -1) {
                    response.getOutputStream().write(ch);
                }
            }
        } else {
            try (BufferedInputStream in = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream("no_image.jpg"))) {
                int ch;
                while ((ch = in.read()) != -1) {
                    response.getOutputStream().write(ch);
                }
            }
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
