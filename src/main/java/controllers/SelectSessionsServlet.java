package controllers;

import dto.SessionDTO;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SelectSessionsServlet", urlPatterns = "/selectsessions")
public class SelectSessionsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<SessionDTO> sessions = SessionServiceImpl.getInstance().getAll();
        if (sessions != null) {
            request.getSession().setAttribute("sessions", sessions);
        } else {
            request.getSession().setAttribute("message", "Not found");
        }
        response.sendRedirect(request.getContextPath() + "/pages/admin/adminSessions.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
