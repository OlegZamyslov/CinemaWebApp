package controllers;

import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@WebServlet(name = "MoviesListServlet", urlPatterns = "/schedule")
public class MoviesListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String term = request.getParameter("term");
        if ((term != null) && ((term.equals("today")) || (term.equals("week")))) {
            int days = (term.equals("today") ? 1 : 7);
            LocalDateTime begin = LocalDateTime.now();
            LocalDateTime end = LocalDate.now().atStartOfDay().plusDays(days).minusSeconds(1);
            List<SessionDTO> sessions = null;
            try {
                sessions = SessionServiceImpl.getInstance().getBetween(begin, end);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.getSession().setAttribute("sessions", sessions);
            request.getSession().setAttribute("term", "");
            response.sendRedirect(request.getContextPath() + "/pages/common/sessions.jsp");
        } else {
            List<MovieDTO> movies = MovieServiceImpl.getInstance().getAll();
            if (movies != null) {
                request.getSession().setAttribute("movieslist", movies);
            } else {
                request.getSession().setAttribute("message", "Not found");
            }
            response.sendRedirect(request.getContextPath() + "/pages/common/movies.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
