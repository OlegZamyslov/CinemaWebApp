package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "MoviesServlet", urlPatterns = "/moviesCRUD")
public class MoviesServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("add") != null) {
            request.getSession().setAttribute("movie", null);
            response.sendRedirect(request.getContextPath() + "/pages/admin/addMovie.jsp");
        } else if (request.getParameter("delete") != null) {
            String[] checkboxs = request.getParameterValues("movieId");
            for (String id : checkboxs) {
                MovieServiceImpl.getInstance().delete(Integer.valueOf(id));
            }
            List<MovieDTO> movies = MovieServiceImpl.getInstance().getAll();
            request.getSession().setAttribute("movies", movies);
            response.sendRedirect(request.getContextPath() + "/pages/admin/adminMovies.jsp");
        } else if (request.getParameter("update") != null) {
            String[] checkboxs = request.getParameterValues("movieId");
            for (String id : checkboxs) {
                MovieDTO movie = MovieServiceImpl.getInstance().getById(Integer.valueOf(id));
                request.getSession().setAttribute("movie", movie);
                String path = "c:/image/" + movie.getId() + ".jpg";
                request.getSession().setAttribute("path", path);
                response.sendRedirect(request.getContextPath() + "/pages/admin/addMovie.jsp");
            }
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
