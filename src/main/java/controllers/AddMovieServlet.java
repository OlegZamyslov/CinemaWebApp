package controllers;

import dto.MovieDTO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Iterator;
import java.util.List;


@WebServlet(name = "AddMovieServlet", urlPatterns = "/addmovie")
public class AddMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = "";
        String description = "";
        String action = "";
        int duration = 0;
        FileItem poster = null;
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        try {
            List<FileItem> items = upload.parseRequest(request);
            Iterator<FileItem> iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = iter.next();
                if (item.isFormField()) {
                    String fieldName = item.getFieldName();
                    String value = item.getString("UTF-8");
                    switch (fieldName) {
                        case "name":
                            name = value;
                            break;
                        case "descr":
                            description = value;
                            break;
                        case "duration":
                            duration = Integer.valueOf(value);
                            break;
                        case "update":
                            action = value;
                            break;
                        case "save":
                            action = value;
                            break;
                    }
                } else {
                    poster = item;
                }
            }
            if (action.equals("Изменить")) {
                MovieDTO movie = (MovieDTO) request.getSession().getAttribute("movie");
                MovieDTO newMovie = new MovieDTO(name, description, duration);
                newMovie.setId(movie.getId());
                MovieServiceImpl.getInstance().update(newMovie);
                File uploadedFile = new File(getServletContext().getContextPath() + "/image/" + movie.getId() + ".jpg");
                uploadedFile.getParentFile().mkdirs();
                poster.write(uploadedFile);
                List<MovieDTO> movies = MovieServiceImpl.getInstance().getAll();
                request.getSession().setAttribute("movies", movies);
            } else if (action.equals("Сохранить")) {
                MovieDTO newMovie = new MovieDTO(name, description, duration);
                List<MovieDTO> movies = (List<MovieDTO>) request.getSession().getAttribute("movies");
                int id = MovieServiceImpl.getInstance().save(newMovie);
                newMovie.setId(id);
                File uploadedFile = new File(getServletContext().getContextPath() + "/image/" + id + ".jpg");
                uploadedFile.getParentFile().mkdirs();
                poster.write(uploadedFile);
                movies.add(newMovie);
                request.getSession().setAttribute("movies", movies);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect(request.getContextPath() + "/pages/admin/adminMovies.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
