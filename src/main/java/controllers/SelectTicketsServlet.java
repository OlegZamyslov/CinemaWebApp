package controllers;

import dto.TicketDTO;
import dto.UserDTO;
import service.impl.TicketServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SelectTicketsServlet", urlPatterns = "/selecttickets")
public class SelectTicketsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<TicketDTO> tickets = TicketServiceImpl.getInstance().getAll();
        if (tickets != null) {
            request.getSession().setAttribute("tickets", tickets);
        }
        response.sendRedirect(request.getContextPath() + "/pages/admin/adminTickets.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
