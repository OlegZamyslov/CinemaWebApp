package controllers;

import dao.impl.TicketDaoImpl;
import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import dto.TicketDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SessionsServlet", urlPatterns = "/sessions")
public class SessionsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("add") != null) {
            List<MovieDTO> movies = MovieServiceImpl.getInstance().getAll();
            request.getSession().setAttribute("movies", movies);
            List<HallDTO> halls = HallServiceImpl.getInstance().getAll();
            request.getSession().setAttribute("halls", halls);
            response.sendRedirect(request.getContextPath() + "/pages/admin/addSession.jsp");
        } else if (request.getParameter("delete") != null) {
            String[] checkboxs = request.getParameterValues("sessionId");
            for (String id : checkboxs) {
                TicketDTO tickets = TicketServiceImpl.getInstance().getBy("session_id", id);
                if (tickets == null) {
                    SessionServiceImpl.getInstance().delete(Integer.valueOf(id));
                    List<SessionDTO> sessions = SessionServiceImpl.getInstance().getAll();
                    request.getSession().setAttribute("sessions", sessions);
                    response.sendRedirect(request.getContextPath() + "/pages/admin/adminSessions.jsp");
                } else {
                    request.getSession().setAttribute("errormessage", "Сначала удалите билеты с этого сеанса");
                    response.sendRedirect(request.getContextPath() + "/pages/common/error.jsp");
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
