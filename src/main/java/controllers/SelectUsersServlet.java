package controllers;

import dto.UserDTO;
import service.impl.UserServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SelectUsersServlet", urlPatterns = "/selectusers")
public class SelectUsersServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<UserDTO> users = UserServiceImpl.getInstance().getAll();
        if (users != null) {
            request.getSession().setAttribute("users", users);
        } else {
            request.getSession().setAttribute("message", "Not found");
        }
        response.sendRedirect(request.getContextPath() + "/pages/admin/adminUsers.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
