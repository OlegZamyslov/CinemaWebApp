package controllers;

import dto.TicketDTO;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "TicketServlet", urlPatterns = "/tickets")
public class TicketServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("delete") != null) {
            String[] checkboxs = request.getParameterValues("ticketId");
            for (String id : checkboxs) {
                TicketServiceImpl.getInstance().delete(Integer.valueOf(id));
            }
            List<TicketDTO> tickets = TicketServiceImpl.getInstance().getAll();
            request.getSession().setAttribute("tickets", tickets);
            response.sendRedirect(request.getContextPath() + "/pages/admin/adminTickets.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
