package controllers;

import dto.HallDTO;
import dto.SessionDTO;
import service.impl.HallServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "HallsServlet", urlPatterns = "/halls")
public class HallsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("add") != null) {
            request.getSession().setAttribute("halls", null);
            request.getSession().setAttribute("hall", null);
            request.getSession().setAttribute("hallstructure", null);
            response.sendRedirect(request.getContextPath() + "/pages/admin/addHall.jsp");
        } else if (request.getParameter("delete") != null) {
            String[] checkboxs = request.getParameterValues("hallId");
            for (String id : checkboxs) {
                SessionDTO listSession = SessionServiceImpl.getInstance().getBy("hall_id", id);
                if (listSession == null) {
                    HallServiceImpl.getInstance().delete(Integer.valueOf(id));
                    List<HallDTO> halls = HallServiceImpl.getInstance().getAll();
                    request.getSession().setAttribute("halls", halls);
                    response.sendRedirect(request.getContextPath() + "/pages/admin/adminHalls.jsp");
                } else {
                    request.getSession().setAttribute("errormessage", "Сначала удалите сеансы");
                    response.sendRedirect(request.getContextPath() + "/pages/common/error.jsp");
                }
            }
        } else if (request.getParameter("update") != null) {
            String[] checkboxs = request.getParameterValues("hallId");
            if (checkboxs != null) {
                HallDTO hall = HallServiceImpl.getInstance().getById(Integer.valueOf(checkboxs[0]));
                List<Integer> hallstructure = Arrays.stream(hall.getSeats()).boxed().collect(Collectors.toList());
                request.getSession().setAttribute("hall", hall);
                request.getSession().setAttribute("hallstructure", hallstructure);
                response.sendRedirect(request.getContextPath() + "/pages/admin/addHall.jsp");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
