package controllers;

import dto.TicketDTO;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


@WebServlet(name = "TicketListServlet", urlPatterns = "/usertickets")
public class TicketListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        if (id != null) {
            List<TicketDTO> tickets = null;
            try {
                tickets = TicketServiceImpl.getInstance().getTickets(Integer.valueOf(id));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.getSession().setAttribute("tickets", tickets);
            response.sendRedirect(request.getContextPath() + "/pages/common/userTickets.jsp");


        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
