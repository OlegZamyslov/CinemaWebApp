package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SelectMoviesServlet", urlPatterns = "/selectmovies")
public class SelectMoviesServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<MovieDTO> movies = MovieServiceImpl.getInstance().getAll();
        if (movies != null) {
            request.getSession().setAttribute("movies", movies);
        } else {
            request.getSession().setAttribute("message", "Not found");
        }
        response.sendRedirect(request.getContextPath() + "/pages/admin/adminMovies.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
