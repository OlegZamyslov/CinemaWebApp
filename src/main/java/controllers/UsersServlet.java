package controllers;

import dto.TicketDTO;
import dto.UserDTO;
import service.impl.TicketServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "UsersServlet", urlPatterns = "/users")
public class UsersServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("add") != null) {
            response.sendRedirect(request.getContextPath() + "/pages/common/addUser.jsp");
        } else if (request.getParameter("delete") != null) {
            String[] checkboxs = request.getParameterValues("userId");
            for (String id : checkboxs) {
                TicketDTO listTicket = TicketServiceImpl.getInstance().getBy("user_id", id);
                if (listTicket == null) {
                    UserServiceImpl.getInstance().delete(Integer.valueOf(id));
                    List<UserDTO> users = UserServiceImpl.getInstance().getAll();
                    request.getSession().setAttribute("users", users);
                    response.sendRedirect(request.getContextPath() + "/pages/admin/adminUsers.jsp");
                } else {
                    request.getSession().setAttribute("errormessage", "Сначала удалите билеты");
                    response.sendRedirect(request.getContextPath() + "/pages/common/error.jsp");
                }
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
