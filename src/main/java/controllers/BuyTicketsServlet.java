package controllers;

import dto.SessionDTO;
import dto.TicketDTO;
import dto.UserDTO;
import service.impl.RoleServiceImpl;
import service.impl.SessionServiceImpl;
import service.impl.TicketServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "buyTicketServlet", urlPatterns = "/buyTicket")
public class BuyTicketsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("savetickets") != null) {
            int sessionId = (int) (request.getSession().getAttribute("id"));
            SessionDTO session = SessionServiceImpl.getInstance().getById(sessionId);
            UserDTO user;
            if (request.getSession().getAttribute("user") != null) {
                user = (UserDTO) request.getSession().getAttribute("user");
            } else {
                user = UserServiceImpl.getInstance().getByLogin("User");
                if (user == null) {
                    user = new UserDTO("User", "userpass", "User", "User", RoleServiceImpl.getInstance().getByName("User"), "User", LocalDate.now());
                    UserServiceImpl.getInstance().save(user);
                }
            }
            String[] checkboxs = request.getParameterValues("seatId");
            List<TicketDTO> myTickets = new ArrayList<>();
            for (String id : checkboxs) {
                int seat = Integer.valueOf(id.substring(id.indexOf('/') + 1));
                int row = Integer.valueOf(id.substring(0, id.indexOf('/')));
                TicketDTO ticket = new TicketDTO(user, row, seat, session);
                TicketServiceImpl.getInstance().save(ticket);
                myTickets.add(ticket);
            }
            request.getSession().setAttribute("myTickets", myTickets);
            response.sendRedirect(request.getContextPath() + "/pages/common/printTicket.jsp");
        } else if (request.getParameter("printtickets") != null) {
            String[] checkboxs = request.getParameterValues("ticketId");
            List<TicketDTO> myTickets = new ArrayList<>();
            for (String id : checkboxs) {
                TicketDTO ticket = TicketServiceImpl.getInstance().getById(Integer.valueOf(id));
                myTickets.add(ticket);
            }
            request.getSession().setAttribute("myTickets", myTickets);
            response.sendRedirect(request.getContextPath() + "/pages/common/printTicket.jsp");

        } else {
            int sessionId = Integer.valueOf(request.getParameter("id"));
            SessionDTO session = SessionServiceImpl.getInstance().getById(sessionId);
            int[] seats = session.getHall().getSeats();
            int[][] freeSeats = new int[seats.length][];
            try {
                List<String> usedSeats = TicketServiceImpl.getInstance().getAllInSession(sessionId);
                freeSeats = getFreeSeats(seats, usedSeats);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.getSession().setAttribute("structure", freeSeats);
            request.getSession().setAttribute("id", sessionId);
            response.sendRedirect(request.getContextPath() + "/pages/common/buyTicket.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected int[][] getFreeSeats(int[] seats, List<String> usedSeats) {
        int[][] freeSeats = new int[seats.length][];
        for (int i = 0; i < seats.length; i++) {
            int[] row = new int[seats[i]];
            freeSeats[i] = row;
        }
        for (String place : usedSeats) {
            int seat = Integer.valueOf(place.substring(place.indexOf('/') + 1));
            int row = Integer.valueOf(place.substring(0, place.indexOf('/')));
            freeSeats[row - 1][seat - 1] = 1;
        }
        return freeSeats;
    }
}
