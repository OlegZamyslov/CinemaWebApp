package controllers;

import dto.HallDTO;
import service.impl.HallServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@WebServlet(name = "AddHallServlet", urlPatterns = "/addhall")
public class AddHallServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("addhall") != null) {
            String name = request.getParameter("Имя зала");
            HallDTO newhall = new HallDTO(name, null);
            request.getSession().setAttribute("hall", newhall);
            response.sendRedirect(request.getContextPath() + "/pages/admin/addStructure.jsp");
        } else if (request.getParameter("addrow") != null) {
            List<Integer> hallstructure = (List<Integer>) request.getSession().getAttribute("hallstructure");
            String parametr = request.getParameter("Мест в ряду");
            if (hallstructure == null) {
                hallstructure = new ArrayList();
            }
            if (!parametr.equals("")) {
                int raw = Integer.valueOf(parametr);
                hallstructure.add(raw);
            }
            request.getSession().setAttribute("hallstructure", hallstructure);
            response.sendRedirect(request.getContextPath() + "/pages/admin/addStructure.jsp");
        } else if (request.getParameter("deleterow") != null) {
            String[] checkboxs = request.getParameterValues("rawId");
            List<Integer> hallstructure = (List<Integer>) request.getSession().getAttribute("hallstructure");
            for (String id : checkboxs) {
                hallstructure.remove(Integer.valueOf(id).intValue() - 1);
            }
            request.getSession().setAttribute("hallstructure", hallstructure);
            response.sendRedirect(request.getContextPath() + "/pages/admin/addStructure.jsp");
        } else if (request.getParameter("addstructure") != null) {
            HallDTO hall = (HallDTO) request.getSession().getAttribute("hall");
            String name = request.getParameter("Имя зала");
            hall.setName(name);
            request.getSession().setAttribute("hall", hall);
            int[] array = hall.getSeats();
            List<Integer> hallstructure = Arrays.stream(array).boxed().collect(Collectors.toList());
            request.getSession().setAttribute("hallstructure", hallstructure);
            response.sendRedirect(request.getContextPath() + "/pages/admin/addStructure.jsp");
        } else if (request.getParameter("savehall") != null) {
            List<Integer> hallstructure = (List<Integer>) request.getSession().getAttribute("hallstructure");
            HallDTO hall = (HallDTO) request.getSession().getAttribute("hall");
            int[] seats = hallstructure.stream().mapToInt(i -> i).toArray();
            hall.setSeats(seats);
            if (hall.getId() == null) {
                HallServiceImpl.getInstance().save(hall);
                List<HallDTO> halls = HallServiceImpl.getInstance().getAll();
                request.getSession().setAttribute("halls", halls);
            } else {
                HallServiceImpl.getInstance().update(hall);
                List<HallDTO> halls = HallServiceImpl.getInstance().getAll();
                request.getSession().setAttribute("halls", halls);
            }
            response.sendRedirect(request.getContextPath() + "/pages/admin/adminHalls.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
