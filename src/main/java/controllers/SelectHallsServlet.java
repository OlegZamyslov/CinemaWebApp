package controllers;

import dto.HallDTO;
import service.impl.HallServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SelectHallsServlet", urlPatterns = "/selecthalls")
public class SelectHallsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<HallDTO> halls = HallServiceImpl.getInstance().getAll();
        if (halls != null) {
            request.getSession().setAttribute("halls", halls);
        } else {
            request.getSession().setAttribute("message", "Not found");
        }
        response.sendRedirect(request.getContextPath() + "/pages/admin/adminHalls.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
