package dao;


import dao.api.Dao;
import dao.impl.*;
import helper.PropertyHolder;
import model.*;


public class DaoFactory {
    private static DaoFactory instance = null;

    private Dao<Integer, Movie> movieDao;
    private Dao<Integer, Hall> hallDao;
    private Dao<Integer, Session> sessionDao;
    private Dao<Integer, Ticket> ticketDao;
    private Dao<Integer, User> userDao;
    private Dao<Integer, Role> roleDao;
    private Dao<Integer, Structure> structureDao;

    private DaoFactory() {
        loadDaos();
    }

    public static DaoFactory getInstance() {
        if (instance == null) {
            instance = new DaoFactory();
        }
        return instance;
    }

    private void loadDaos() {
        if (PropertyHolder.getInstance().isInMemoryDB()) {

        } else {
            movieDao = MovieDaoImpl.getInstance();
            hallDao = HallDaoImpl.getInstance();
            sessionDao = SessionDaoImpl.getInstance();
            ticketDao = TicketDaoImpl.getInstance();
            userDao = UserDaoImpl.getInstance();
            roleDao = RoleDaoImpl.getInstance();
            structureDao = StructureDaoImpl.getInstance();
        }
    }

    public Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }

    public void setMovieDao(Dao<Integer, Movie> movieDao) {
        this.movieDao = movieDao;
    }

    public Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }

    public void setHallDao(Dao<Integer, Hall> hallDao) {
        this.hallDao = hallDao;
    }

    public Dao<Integer, Session> getSessionDao() {
        return sessionDao;
    }

    public void setSessionDao(Dao<Integer, Session> sessionDao) {
        this.sessionDao = sessionDao;
    }

    public Dao<Integer, Ticket> getTicketDao() {
        return ticketDao;
    }

    public void setTicketDao(Dao<Integer, Ticket> ticketDao) {
        this.ticketDao = ticketDao;
    }

    public Dao<Integer, User> getUserDao() {
        return userDao;
    }

    public void setUserDao(Dao<Integer, User> userDao) {
        this.userDao = userDao;
    }

    public Dao<Integer, Role> getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(Dao<Integer, Role> roleDao) {
        this.roleDao = roleDao;
    }

    public Dao<Integer, Structure> getStructureDao() {
        return structureDao;
    }

    public void setStructureDao(Dao<Integer, Structure> structureDao) {
        this.structureDao = structureDao;
    }
}
