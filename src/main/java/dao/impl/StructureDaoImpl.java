package dao.impl;

import datasource.DataSource;
import model.Structure;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class StructureDaoImpl extends CrudDAO<Structure> {
    private final String INSERT = "Insert into structure (hall_id,row,seat) values (?,?,?)";
    private final String UPDATE = "UPDATE structure SET hall_id = ?,row = ?,seat = ? WHERE id = ?";
    public  final String DELETE = "DELETE FROM structure WHERE hall_id = ?";
    public  final String DELETE_ROW = "DELETE FROM structure WHERE (hall_id = ? and row = ?)";

    private static StructureDaoImpl crudDAO;

    private StructureDaoImpl(Class type) {
        super(type);
    }


    public static synchronized StructureDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new StructureDaoImpl(Structure.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Structure entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        //preparedStatement.setInt(1, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, Structure entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        return preparedStatement;
    }

    @Override
    public List<Structure> readAll(ResultSet resultSet) throws SQLException {
        List<Structure> result = new LinkedList<>();
        Structure structure = new Structure();
        List<Integer> rows = new ArrayList<>();
        while (resultSet.next()) {
            rows.add(resultSet.getInt("seat"));
            structure.setId(resultSet.getInt("id"));
            structure.setHall_id(resultSet.getInt("hall_id"));
        }
        int[] seats = new int[rows.size()];
        for (int i = 0; i < rows.size(); i++) {
            seats[i] = rows.get(i);
        }
        structure.setSeats(seats);
        result.add(structure);
        return result;
    }

    @Override
    public void save(Structure entity) {
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = createInsertStatement(connection, entity)) {
            if (entity.getSeats() != null) {
                preparedStatement.setInt(1, entity.getHall_id());
                int[] structure = entity.getSeats();
                for (int i = 0; i < structure.length; i++) {
                    preparedStatement.setInt(2, i + 1);
                    preparedStatement.setInt(3, structure[i]);
                    preparedStatement.executeUpdate();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void update (Structure entity) {
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, entity.getHall_id());
            preparedStatement.executeUpdate();
            save(entity);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteRow (Structure entity, int row) {
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ROW)) {
            preparedStatement.setInt(1, entity.getHall_id());
            preparedStatement.setInt(2, row);
            preparedStatement.executeUpdate();
            save(entity);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Integer key) {
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, key);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}