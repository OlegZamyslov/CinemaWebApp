package dao.impl;

import datasource.DataSource;
import model.Hall;
import model.Movie;
import model.Session;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;


public class SessionDaoImpl extends CrudDAO<Session> {
    private final String INSERT = "Insert into SESSION (movie_id, hall_id, sessiondate, price) values (?,?,?,?)";
    private final String UPDATE = "UPDATE SESSION SET movie_id = ?, hall_id = ?, " +
            "sessiondate = ?, price = ? WHERE id = ?";
    private final String SELECT_BETWEEN = "Select * from SESSION WHERE sessiondate BETWEEN ? AND ?";
    private static SessionDaoImpl crudDAO;

    private SessionDaoImpl(Class type) {
        super(type);
    }


    public static synchronized SessionDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new SessionDaoImpl(Session.class);
        }
        return crudDAO;
    }


    public List<Session> getBetween(LocalDateTime begin, LocalDateTime end) throws SQLException {
        List<Session> result = null;
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BETWEEN);) {
            preparedStatement.setTimestamp(1, Timestamp.valueOf(begin));
            preparedStatement.setTimestamp(2, Timestamp.valueOf(end));
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, entity.getMovie().getId());
        preparedStatement.setInt(2, entity.getHall().getId());
        preparedStatement.setTimestamp(3, Timestamp.valueOf(entity.getSessionDate()));
        preparedStatement.setInt(4, entity.getPrice());
        preparedStatement.setInt(5, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getMovie().getId());
        preparedStatement.setInt(2, entity.getHall().getId());
        preparedStatement.setTimestamp(3, Timestamp.valueOf(entity.getSessionDate()));
        preparedStatement.setInt(4, entity.getPrice());
        return preparedStatement;
    }

    @Override
    public List<Session> readAll(ResultSet resultSet) throws SQLException {
        List<Session> result = new LinkedList<>();
        Session session;
        while (resultSet.next()) {
            session = new Session();
            session.setId(resultSet.getInt("id"));
            session.setPrice(resultSet.getInt("price"));
            session.setSessionDate(resultSet.getTimestamp("sessiondate").toLocalDateTime());
            Movie movie = MovieDaoImpl.getInstance().getById(resultSet.getInt("movie_id"));
            session.setMovie(movie);
            Hall hall = HallDaoImpl.getInstance().getById(resultSet.getInt("hall_id"));
            session.setHall(hall);

            result.add(session);
        }
        return result;
    }

}
