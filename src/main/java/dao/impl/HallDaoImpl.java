package dao.impl;

import datasource.DataSource;
import model.Hall;
import model.Structure;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public class HallDaoImpl extends CrudDAO<Hall> {
    private final String INSERT = "Insert into hall (name) values (?)";
    private final String UPDATE = "UPDATE hall SET name = ? WHERE id = ?";
    public static final String DELETE = "DELETE FROM hall WHERE id = ?";

    private static HallDaoImpl crudDAO;

    private HallDaoImpl(Class type) {
        super(type);
    }


    public static synchronized HallDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new HallDaoImpl(Hall.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        return preparedStatement;
    }

    @Override
    public List<Hall> readAll(ResultSet resultSet) throws SQLException {
        List<Hall> result = new LinkedList<>();
        Hall hall;
        while (resultSet.next()) {
            hall = new Hall();
            hall.setId(resultSet.getInt("id"));
            hall.setName(resultSet.getString("name"));
            Structure structure = StructureDaoImpl.getInstance().getBy("hall_id",hall.getId().toString());
            hall.setSeats(structure.getSeats());
            result.add(hall);
        }
        return result;
    }

    @Override
    public void save(Hall entity) {
        super.save(entity);
        Structure structure = new Structure(entity.getId(),entity.getSeats());
        StructureDaoImpl.getInstance().save(structure);
    }

    @Override
    public void update(Hall entity) {
        super.update(entity);
        Structure structure = new Structure(entity.getId(),entity.getSeats());
        StructureDaoImpl.getInstance().update(structure);
    }

    @Override
    public void delete(Integer key) {
        StructureDaoImpl.getInstance().delete(key);
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, key);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}