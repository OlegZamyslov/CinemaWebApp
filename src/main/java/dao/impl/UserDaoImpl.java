package dao.impl;

import model.Role;
import model.User;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public final class UserDaoImpl extends CrudDAO<User> {
    private final String INSERT = "Insert into USER (login, pass, firstName, lastName, role_id , " +
            "email, bday) values (?,?,?,?,?,?,?)";
    private final String UPDATE = "UPDATE USER SET login = ?, pass = ?, " +
            "firstName = ?, lastName = ?, role_id = ?, email = ?, bday = ? WHERE id = ?";
    private static UserDaoImpl crudDAO;

    private UserDaoImpl(Class type) {
        super(type);
    }


    public static synchronized UserDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new UserDaoImpl(User.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPass());
        preparedStatement.setString(3, entity.getFirstName());
        preparedStatement.setString(4, entity.getLastName());
        preparedStatement.setInt(5, entity.getRole().getId());
        preparedStatement.setString(6, entity.getEmail());
        preparedStatement.setString(7, entity.getBDay().toString());
        preparedStatement.setInt(8, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPass());
        preparedStatement.setString(3, entity.getFirstName());
        preparedStatement.setString(4, entity.getLastName());
        preparedStatement.setInt(5, entity.getRole().getId());
        preparedStatement.setString(6, entity.getEmail());
        preparedStatement.setString(7, entity.getBDay().toString());
        return preparedStatement;
    }

    @Override
    public List<User> readAll(ResultSet resultSet) throws SQLException {
        List<User> result = new LinkedList<>();
        User user;
        while (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPass(resultSet.getString("pass"));
            user.setFirstName(resultSet.getString("firstName"));
            user.setLastName(resultSet.getString("lastName"));
            user.setLastName(resultSet.getString("lastName"));
            user.setBDay(resultSet.getDate("bday").toLocalDate());
            user.setEmail(resultSet.getString("email"));
            Role role = RoleDaoImpl.getInstance().getById(resultSet.getInt("role_id"));
            user.setRole(role);
            result.add(user);
        }
        return result;
    }

}