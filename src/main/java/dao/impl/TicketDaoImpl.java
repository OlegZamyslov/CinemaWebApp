package dao.impl;

import datasource.DataSource;
import model.Session;
import model.Ticket;
import model.User;

import java.sql.*;
import java.util.*;


public class TicketDaoImpl extends CrudDAO<Ticket> {
    private final String INSERT = "Insert into TICKET (user_id, row, seat, session_id) values (?,?,?,?)";
    private final String UPDATE = "UPDATE TICKET SET user_id = ?, row = ?,seat = ?, session_id = ? WHERE id = ?";
    private final String SELECT_IN_SESSION = "Select * from TICKET WHERE session_id = ?";
    private final String SELECT_BY_USER = "Select * from TICKET WHERE user_id = ?";

    private static TicketDaoImpl crudDAO;

    private TicketDaoImpl(Class type) {
        super(type);
    }


    public static synchronized TicketDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new TicketDaoImpl(Ticket.class);
        }
        return crudDAO;
    }

    public List<Ticket> getTickets(int id) throws SQLException {
        List<Ticket> result = null;
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_USER)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
            Collections.sort(result, (a,b)->b.getSession().getSessionDate().compareTo(a.getSession().getSessionDate()));
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, entity.getUser().getId());
        preparedStatement.setInt(2, entity.getRow());
        preparedStatement.setInt(3, entity.getSeat());
        preparedStatement.setInt(4, entity.getSession().getId());
        preparedStatement.setInt(5, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        if (entity.getUser() != null) {
            preparedStatement.setInt(1, entity.getUser().getId());
        } else {
            preparedStatement.setInt(1, 0);
        }
        preparedStatement.setInt(2, entity.getRow());
        preparedStatement.setInt(3, entity.getSeat());
        preparedStatement.setInt(4, entity.getSession().getId());
        return preparedStatement;
    }

    @Override
    public List<Ticket> readAll(ResultSet resultSet) throws SQLException {
        List<Ticket> result = new LinkedList<>();
        Ticket ticket;
        while (resultSet.next()) {
            ticket = new Ticket();
            ticket.setId(resultSet.getInt("id"));
            ticket.setRow(resultSet.getInt("row"));
            ticket.setSeat(resultSet.getInt("seat"));
            User user = UserDaoImpl.getInstance().getById(resultSet.getInt("user_id"));
            ticket.setUser(user);
            Session session = SessionDaoImpl.getInstance().getById(resultSet.getInt("session_id"));
            ticket.setSession(session);
            result.add(ticket);
        }
        return result;
    }

    public List<String> getAllInSession(Integer id) throws SQLException {
        List<String> result = new ArrayList<>();
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_IN_SESSION)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int row = resultSet.getInt("row");
                int seat = resultSet.getInt("seat");
                result.add(row + "/" + seat);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

}
