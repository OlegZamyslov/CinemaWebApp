package simple;

import dto.RoleDTO;
import dto.UserDTO;
import service.impl.RoleServiceImpl;
import service.impl.UserServiceImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

public class App {
    private final static String URL = "jdbc:mysql://localhost:3306/cinema_database?useSSL=false";
//    private final static String URL = "jdbc:mysql://localhost:3306/my_database?useSSL=false";
    private final static String USER = "root";
    private final static String PASSWORD = "root";
    private final static String SQL_CREATE_HALL = "CREATE TABLE IF NOT EXISTS hall(id int AUTO_INCREMENT primary key,name VARCHAR(40))";
    private final static String SQL_CREATE_MOVIE = "CREATE TABLE IF NOT EXISTS movie(id int AUTO_INCREMENT primary key, " +
            "title VARCHAR(40), description VARCHAR(40), duration int)";
    private final static String SQL_CREATE_ROLE = "CREATE TABLE IF NOT EXISTS role(id int AUTO_INCREMENT primary key, " +
            "name VARCHAR(10))";
    private final static String SQL_CREATE_USER = "CREATE TABLE IF NOT EXISTS user(id int AUTO_INCREMENT primary key, " +
            "login VARCHAR(20),pass VARCHAR(20),firstName VARCHAR(20),lastName VARCHAR(20)," +
            "role_id int, FOREIGN KEY (role_id) REFERENCES role(id), " +
            "email VARCHAR(30), bday DATE)";
    private final static String SQL_CREATE_SESSION = "CREATE TABLE IF NOT EXISTS session(id int AUTO_INCREMENT primary key, " +
            "movie_id int,FOREIGN KEY (movie_id) REFERENCES movie(id)," +
            "hall_id INT, FOREIGN KEY (hall_id) REFERENCES hall(id),sessiondate TIMESTAMP ,price int)";
    private final static String SQL_CREATE_STRUCTURE = "CREATE TABLE IF NOT EXISTS structure(id int AUTO_INCREMENT primary key, " +
            "hall_id INT, FOREIGN KEY (hall_id) REFERENCES hall(id),row int,seat int)";
    private final static String SQL_CREATE_TICKET = "CREATE TABLE IF NOT EXISTS ticket(id int AUTO_INCREMENT primary key, " +
            "user_id int, FOREIGN KEY (user_id) REFERENCES user(id)," +
            "session_id int, FOREIGN KEY (session_id) REFERENCES session(id),row int,seat int)";

    public static void main(String[] args) {
        try(Connection connection = DriverManager.getConnection(URL,USER, PASSWORD);
            Statement statement = connection.createStatement()){
            statement.execute(SQL_CREATE_ROLE);
            statement.execute(SQL_CREATE_HALL);
            statement.execute(SQL_CREATE_STRUCTURE);
            statement.execute(SQL_CREATE_MOVIE);
            statement.execute(SQL_CREATE_USER);
            statement.execute(SQL_CREATE_SESSION);
            statement.execute(SQL_CREATE_TICKET);

            RoleDTO role = new RoleDTO("Admin");
            RoleServiceImpl.getInstance().save(role);
            RoleDTO user = new RoleDTO("User");
            RoleServiceImpl.getInstance().save(user);
            UserDTO admin = new UserDTO("Admin","admin","Admin","Admin",RoleServiceImpl.getInstance().getByName("Admin"),"Admin", LocalDate.now());
            UserServiceImpl.getInstance().save(admin);
            UserDTO user2 = new UserDTO("User","Userpass","User","User",RoleServiceImpl.getInstance().getByName("User"),"User", LocalDate.now());
            UserServiceImpl.getInstance().save(user2);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
