package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dao.impl.SessionDaoImpl;
import dto.SessionDTO;
import mapper.BeanMapper;
import model.Session;
import service.api.Service;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;


public class SessionServiceImpl implements Service<Integer, SessionDTO> {

    private static SessionServiceImpl service;
    private Dao<Integer, Session> sessionDao;
    private BeanMapper beanMapper;

    private SessionServiceImpl() {
        sessionDao = DaoFactory.getInstance().getSessionDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized SessionServiceImpl getInstance() {
        if (service == null) {
            service = new SessionServiceImpl();
        }
        return service;
    }


    @Override
    public List<SessionDTO> getAll() {
        List<Session> sessions = sessionDao.getAll();
        List<SessionDTO> sessionDTOs = beanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }

    @Override
    public int save(SessionDTO sessionDto) {
        Session session = beanMapper.singleMapper(sessionDto, Session.class);
        sessionDao.save(session);
        return session.getId();
    }

    @Override
    public SessionDTO getById(Integer id) {
        Session session = sessionDao.getById(id);
        SessionDTO sessionDTO = beanMapper.singleMapper(session, SessionDTO.class);
        return sessionDTO;
    }

    public List<SessionDTO> getBetween(LocalDateTime begin, LocalDateTime end) throws SQLException {
        List<Session> sessions = SessionDaoImpl.getInstance().getBetween(begin, end);
        List<SessionDTO> result = beanMapper.listMapToList(sessions, SessionDTO.class);
        return result;
    }

    @Override
    public void delete(Integer id) {
        sessionDao.delete(id);
    }

    @Override
    public void update(SessionDTO entity) {
        Session session = beanMapper.singleMapper(entity, Session.class);
        sessionDao.update(session);
    }

    public SessionDTO getBy(String name,String value) {
        Session session = sessionDao.getBy(name,value);
        SessionDTO sessionDTO = beanMapper.singleMapper(session, SessionDTO.class);
        return sessionDTO;
    }

}

