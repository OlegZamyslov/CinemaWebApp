package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.RoleDTO;
import mapper.BeanMapper;
import model.Role;
import service.api.Service;

import java.util.List;


public class RoleServiceImpl implements Service<Integer, RoleDTO> {

    private static RoleServiceImpl service;
    private Dao<Integer, Role> roleDao;
    private BeanMapper beanMapper;

    private RoleServiceImpl() {
        roleDao = DaoFactory.getInstance().getRoleDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized RoleServiceImpl getInstance() {
        if (service == null) {
            service = new RoleServiceImpl();
        }
        return service;
    }


    @Override
    public List<RoleDTO> getAll() {
        List<Role> roles = roleDao.getAll();
        List<RoleDTO> roleDTOs = beanMapper.listMapToList(roles, RoleDTO.class);
        return roleDTOs;
    }

    @Override
    public int save(RoleDTO roleDto) {
        Role role = beanMapper.singleMapper(roleDto, Role.class);
        roleDao.save(role);
        return role.getId();
    }

    @Override
    public RoleDTO getById(Integer id) {
        Role role = roleDao.getById(id);
        RoleDTO roleDTO = beanMapper.singleMapper(role, RoleDTO.class);
        return roleDTO;
    }

    public RoleDTO getByName(String name) {
        Role role = roleDao.getBy("name",name);
        RoleDTO roleDTO = beanMapper.singleMapper(role, RoleDTO.class);
        return roleDTO;
    }

    @Override
    public void delete(Integer id) {
        roleDao.delete(id);
    }

    @Override
    public void update(RoleDTO entity) {
        Role role = beanMapper.singleMapper(entity, Role.class);
        roleDao.update(role);
    }

}
