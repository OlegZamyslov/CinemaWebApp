package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.UserDTO;
import mapper.BeanMapper;
import model.User;
import service.api.Service;

import java.util.List;


public class UserServiceImpl implements Service<Integer, UserDTO> {

    private static UserServiceImpl service;
    private Dao<Integer, User> userDao;
    private BeanMapper beanMapper;

    private UserServiceImpl() {
        userDao = DaoFactory.getInstance().getUserDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized UserServiceImpl getInstance() {
        if (service == null) {
            service = new UserServiceImpl();
        }
        return service;
    }


    @Override
    public List<UserDTO> getAll() {
        List<User> users = userDao.getAll();
        List<UserDTO> UserDTOs = beanMapper.listMapToList(users, UserDTO.class);
        return UserDTOs;
    }

    @Override
    public int save(UserDTO userDto) {
        User user = beanMapper.singleMapper(userDto, User.class);
        userDao.save(user);
        return user.getId();
    }

    @Override
    public UserDTO getById(Integer id) {
        User movie = userDao.getById(id);
        UserDTO UserDTO = beanMapper.singleMapper(movie, UserDTO.class);
        return UserDTO;
    }

    public UserDTO getByLogin(String login) {
        User movie = userDao.getBy("login",login);
        UserDTO UserDTO = beanMapper.singleMapper(movie, UserDTO.class);
        return UserDTO;
    }

    @Override
    public void delete(Integer id) {
        userDao.delete(id);
    }

    @Override
    public void update(UserDTO entity) {
        User user = beanMapper.singleMapper(entity, User.class);
        userDao.update(user);
    }

    public UserDTO getBy(String name,String value) {
        User user = userDao.getBy(name,value);
        UserDTO sessionDTO = beanMapper.singleMapper(user, UserDTO.class);
        return sessionDTO;
    }


}
