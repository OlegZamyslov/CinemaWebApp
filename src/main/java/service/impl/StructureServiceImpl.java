package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dao.impl.StructureDaoImpl;
import dto.StructureDTO;
import mapper.BeanMapper;
import model.Structure;
import service.api.Service;

import java.util.List;


public class StructureServiceImpl implements Service<Integer, StructureDTO> {

    private static StructureServiceImpl service;
    private Dao<Integer, Structure> structureDao;
    private BeanMapper beanMapper;

    private StructureServiceImpl() {
        structureDao = DaoFactory.getInstance().getStructureDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized StructureServiceImpl getInstance() {
        if (service == null) {
            service = new StructureServiceImpl();
        }
        return service;
    }


    @Override
    public List<StructureDTO> getAll() {
        List<Structure> structures = structureDao.getAll();
        List<StructureDTO> structureDTOs = beanMapper.listMapToList(structures, StructureDTO.class);
        return structureDTOs;
    }

    @Override
    public int save(StructureDTO structureDto) {
        Structure structure = beanMapper.singleMapper(structureDto, Structure.class);
        structureDao.save(structure);
        return structure.getId();
    }

    @Override
    public StructureDTO getById(Integer id) {
        Structure structure = structureDao.getById(id);
        StructureDTO structureDTO = beanMapper.singleMapper(structure, StructureDTO.class);
        return structureDTO;
    }

    public void deleterow(Integer id, int row) {
        Structure structure = structureDao.getById(id);
        StructureDaoImpl.getInstance().deleteRow(structure, row);
    }

    @Override
    public void delete(Integer id) {
        structureDao.delete(id);
    }

    @Override
    public void update(StructureDTO entity) {
        Structure structure = beanMapper.singleMapper(entity, Structure.class);
        structureDao.update(structure);
    }

}

