package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dao.impl.TicketDaoImpl;
import dto.TicketDTO;
import mapper.BeanMapper;
import model.Ticket;
import service.api.Service;

import java.sql.SQLException;
import java.util.List;


public class TicketServiceImpl implements Service<Integer, TicketDTO> {

    private static TicketServiceImpl service;
    private Dao<Integer, Ticket> ticketDao;
    private BeanMapper beanMapper;

    private TicketServiceImpl() {
        ticketDao = DaoFactory.getInstance().getTicketDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized TicketServiceImpl getInstance() {
        if (service == null) {
            service = new TicketServiceImpl();
        }
        return service;
    }


    @Override
    public List<TicketDTO> getAll() {
        List<Ticket> tickets = ticketDao.getAll();
        List<TicketDTO> TicketDTOs = beanMapper.listMapToList(tickets, TicketDTO.class);
        return TicketDTOs;
    }

    public List<String> getAllInSession(Integer id) throws SQLException {
        List<String> tickets = TicketDaoImpl.getInstance().getAllInSession(id);
        return tickets;
    }

    @Override
    public int save(TicketDTO ticketDto) {
        Ticket ticket = beanMapper.singleMapper(ticketDto, Ticket.class);
        ticketDao.save(ticket);
        ticketDto.setId(ticket.getId());
        return ticket.getId();
    }

    @Override
    public TicketDTO getById(Integer id) {
        Ticket movie = ticketDao.getById(id);
        TicketDTO ticketDTO = beanMapper.singleMapper(movie, TicketDTO.class);
        return ticketDTO;
    }

    public List<TicketDTO> getTickets(int id) throws SQLException {
        List<Ticket> tickets = TicketDaoImpl.getInstance().getTickets(id);
        List<TicketDTO> result = beanMapper.listMapToList(tickets, TicketDTO.class);
        return result;
    }

    @Override
    public void delete(Integer id) {
        ticketDao.delete(id);
    }

    @Override
    public void update(TicketDTO entity) {
        Ticket ticket = beanMapper.singleMapper(entity, Ticket.class);
        ticketDao.update(ticket);
    }

    public TicketDTO getBy(String name,String value) {
        Ticket ticket = ticketDao.getBy(name,value);
        TicketDTO ticketDTO = beanMapper.singleMapper(ticket, TicketDTO.class);
        return ticketDTO;
    }

}

